import sqlite3
import Person
import front_end

conn = sqlite3.connect('database.db')
cursor = conn.cursor()


def select_user_type():
    while True:
        choice = int(input("Enter:"))
        if choice == 0:
            return Person.Owner
        if choice == 1:
            return Person.User


def insert_in_db(new_user) -> str:
    user_dict = {'username': new_user.Username, 'name': new_user.Name, 'surname': new_user.Surname,
                 'password': new_user.Password, 'birthdate': new_user.Birthdate, 'email': new_user.Email}
    with conn:
        if type(new_user) == Person.Owner:
            user_dict['taxcode'] = new_user.TaxCode
            cursor.execute("INSERT INTO Owner VALUES (:username, :name, :surname, :password, :birthdate, :email, "
                           ":taxcode)", user_dict)
            return "Owner registered successfully"

        cursor.execute("INSERT INTO User VALUES (:username, :name, :surname, :password, :birthdate, :email)", user_dict)
        for i in range(len(new_user.CreditCard)):
            cursor.execute("INSERT INTO CreditCards VALUES (:username, :cardindex, :cardnumber, :expdate, "
                           ":cardholder, :cvc)",
                           {'username': new_user.Username, 'cardindex': i,
                            'cardnumber': new_user.CreditCard[i].CardNumber,
                            'expdate': new_user.CreditCard[i].ExpDate, 'cardholder': new_user.CreditCard[i].CardHolder,
                            'cvc': new_user.CreditCard[i].CVC})
        return "User registered successfully"


def initialize_db():
    with conn:
        cursor.execute("""CREATE TABLE IF NOT EXISTS Owner (
            username text,
            name text,
            surname text,
            password text,
            birthdate text,
            email text,
            tax_code text
            )""")

        cursor.execute("""CREATE TABLE IF NOT EXISTS User (
            username text UNIQUE,
            name text,
            surname text,
            password text,
            birthdate text,
            email text
            )""")

        cursor.execute("""CREATE TABLE IF NOT EXISTS CreditCards (
            username text,
            cardindex integer,
            CardNumber text,
            ExpDate text,
            CardHolder text,
            CVC text
            )""")


def main():
    initialize_db()
    front_end.Interface.BootInterface()
    new = Person.Registration(select_user_type())
    insert_in_db(new)
    # with conn: #prova persistenza
    #    print("users registered till now:\n")
    #    cursor.execute("SELECT * FROM User")
    #    print(cursor.fetchall())
    #    print("owner registered till now:\n")
    #    cursor.execute("SELECT * FROM Owner")
    #    print(cursor.fetchall())
    conn.close()


if __name__ == "__main__":
    main()
