from datetime import datetime
import sqlite3
conn = sqlite3.connect('database.db')
cursor = conn.cursor()


def Registration(_type):
    new_user = _type()
    new_user.Username = input("Enter username:\n")
    new_user.Name = input("Enter name:\n")
    new_user.Surname = input("Enter surname:\n")
    new_user.Password = input("Enter password:\n")
    new_user.Birthdate = input("Enter birthdate (d/m/y):\n")
    new_user.Email = input("Enter email:\n")
    if _type == Owner:
        new_user.TaxCode = input("Enter tax code:\n")
        return new_user
    new_user.CreditCard = int(input("How many credit cards do you want to enter?\n"))
    return new_user


def CreditCardCreator():
    new_cc = CreditCard()
    new_cc.CardNumber = input("Enter the card number:\n")
    new_cc.ExpDate = input("Enter the expiration date:\n")
    new_cc.CardHolder = input("Enter the name of the card holder:\n")
    new_cc.CVC = input("Enter the creditcard CVC:\n")
    return new_cc


class Person:
    def __init__(self, username: str = '', name: str = '', surname: str = '', password: str = '',
                 birthdate: datetime = datetime.today(), email: str = ''):
        self._username = username
        self._name = name
        self._surname = surname
        self._password = password
        self._birthdate = birthdate
        self._email = email

    @property
    def Username(self):
        return self._username

    @Username.setter
    def Username(self, value):
        while True:
            cursor.execute("SELECT * FROM User WHERE username = '{}'".format(value))
            try:
                if cursor.fetchone()[0] == value:
                    print("Username already exist\n")
                    value = input("Enter another Username:\n")
            except TypeError:
                break
        self._username = value

    @property
    def Name(self):
        return self._name

    @Name.setter
    def Name(self, value):
        self._name = value

    @property
    def Surname(self):
        return self._surname

    @Surname.setter
    def Surname(self, value):
        self._surname = value

    @property
    def Password(self):
        return self._password

    @Password.setter
    def Password(self, value):
        self._password = value

    @property
    def Birthdate(self):
        return self._birthdate

    @Birthdate.setter
    def Birthdate(self, value: str):
        self._birthdate = datetime.strptime(value, "%d/%m/%Y").date()

    @property
    def Email(self):
        return self._email

    @Email.setter
    def Email(self, value):
        self._email = value


class Owner(Person):
    def __init__(self, username: str = '', name: str = '', surname: str = '', password: str = '',
                 birthdate: datetime = datetime.today(), email: str = '', tax_code: str = ''):
        super().__init__(username, name, surname, password, birthdate, email)
        self._tax_code = tax_code

    @property
    def TaxCode(self):
        return self._tax_code

    @TaxCode.setter
    def TaxCode(self, value):
        self._tax_code = value


class User(Person):
    def __init__(self, username: str = '', name: str = '', surname: str = '', password: str = '',
                 birthdate: datetime = datetime.today(), email: str = '', creditcards: list = None):
        super().__init__(username, name, surname, password, birthdate, email)
        if creditcards is None:
            self._creditcards = []

    @property
    def CreditCard(self):
        return self._creditcards

    @CreditCard.setter
    def CreditCard(self, value):
        if value == 0:
            self._creditcards = []
        for i in range(value):
            temp_card = CreditCardCreator()
            self._creditcards.append(temp_card)


class CreditCard:
    def __init__(self, card_number: str = '', exp_date: datetime = datetime.today(), card_holder: str = '',
                 cvc_code: int = 0):
        self._card_number = card_number
        self._exp_date = exp_date
        self._card_holder = card_holder
        self._cvc_code = cvc_code

    @property
    def CardNumber(self):
        return self._card_number

    @CardNumber.setter
    def CardNumber(self, value):
        self._card_number = value

    @property
    def ExpDate(self):
        return self._exp_date

    @ExpDate.setter
    def ExpDate(self, value):
        self._exp_date = value

    @property
    def CardHolder(self):
        return self._card_holder

    @CardHolder.setter
    def CardHolder(self, value):
        self._card_holder = value

    @property
    def CVC(self):
        return self._cvc_code

    @CVC.setter
    def CVC(self, value):
        self._cvc_code = value
