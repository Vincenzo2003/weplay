
class Interface:
    _BootInterface = "Registration for:\n0. Owner\n1. User"
    _AdminInterface = "Enter your credentials.\n"
    _UserInterface = "Select what to do.\n0. Login\n1. Registration"

    @classmethod
    def BootInterface(cls):
        return print(cls._BootInterface)

    @classmethod
    def AdminInterface(cls):
        return print(cls._AdminInterface)

    @classmethod
    def UserInterface(cls):
        return print(cls._UserInterface)
